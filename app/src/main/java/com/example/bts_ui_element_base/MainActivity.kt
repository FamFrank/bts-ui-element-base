package com.example.bts_ui_element_base

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.*

class MainActivity : AppCompatActivity() {

    private var TAG:String = "MainActivity"
    private lateinit var button:Button
    private lateinit var button1:Button
    private lateinit var checkBox: CheckBox
    private lateinit var switch: Switch
    private lateinit var imageView: ImageView
    private lateinit var imageView1: ImageView
    private lateinit var imageView2: ImageView
    private lateinit var imageView3: ImageView
    private lateinit var imageView4: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button = findViewById(R.id.button)
        button.setOnClickListener { view ->
            Toast.makeText(this, "You have pressed the button!!!", Toast.LENGTH_SHORT).show()
            Log.d(TAG, "You have pressed the button, here is the log!!")
        }

        button1 = findViewById(R.id.button2)
        button1.setOnClickListener { view ->
            Toast.makeText(this, "I knew you would love these!!", Toast.LENGTH_SHORT).show()
            Log.d(TAG, "You have pressed the button1, here is the log!!")
        }

        checkBox = findViewById(R.id.checkBox)
        checkBox.setOnCheckedChangeListener { buttonview, isChecked ->
            Toast.makeText(this, isChecked.toString(), Toast.LENGTH_SHORT).show()
            Log.d(TAG, "The checkbox has been checked")
        }

        switch = findViewById(R.id.switch1)
        switch.setOnCheckedChangeListener { buttonview, isChecked ->
            Toast.makeText(this, isChecked.toString(), Toast.LENGTH_SHORT).show()
            Log.d(TAG, "The switch has been switched")
        }

        imageView1 = findViewById(R.id.camera)
        imageView1.setOnClickListener { imageView ->
            Toast.makeText(this, "Sorry, no camera feature on this phone!!", Toast.LENGTH_SHORT).show()
            Log.d(TAG, "The camera has been clicked")
        }

        imageView2 = findViewById(R.id.call)
        imageView2.setOnClickListener { imageView ->
            Toast.makeText(this, "Sorry, cannot call on this phone!!", Toast.LENGTH_SHORT).show()
            Log.d(TAG, "A call was attempted")
        }

        imageView3 = findViewById(R.id.facetime)
        imageView3.setOnClickListener { imageView ->
            Toast.makeText(this, "Sorry, no facetime!!", Toast.LENGTH_SHORT).show()
            Log.d(TAG, "Facetime was opened, not")
        }

        imageView4 = findViewById(R.id.star)
        imageView4.setOnClickListener { imageView ->
            Toast.makeText(this, "!!!You are a star!!!", Toast.LENGTH_SHORT).show()
            Log.d(TAG, "A star was born")
        }
        imageView = findViewById(R.id.mic)
        imageView.setOnClickListener { imageView ->
            Toast.makeText(this, "No mic, sorry!!", Toast.LENGTH_SHORT).show()
            Log.d(TAG, "The mic broke")
        }
    }
}